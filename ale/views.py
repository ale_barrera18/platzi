from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
phones = [
    {
    'Model': 'Samsung S10 ',
    'Company':{
        'Name':'Samsung LTDA',
        'Icon' : 'https://cdn.iconscout.com/icon/free/png-256/samsung-226432.png',
        'Direccion': 'Hipolito Salas 1421' 
        },
    'Image': 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
    },

    {
    'Model': 'Iphone X',
    'Company':{
        'name':'Apple Company',
        'Icon' : 'https://image.flaticon.com/icons/png/512/23/23656.png',
        'Direccion':'Providencia 2353'
    },
    'Image': 'https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/main_card_image/https/bdt.computerhoy.com/sites/default/files/iphone-x-apple.png?itok=_xTJdFWI',
    }

]



def winner(request):
    return render(request,'posts/feed.html',{'phones': phones})