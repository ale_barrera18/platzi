from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin
from users.models import Profile
from django.contrib.auth.models import User
# Register your models here.
#admin.site.register(Profile)

#Crear un clase para registra a profile y mostrar los campos que uno quiere

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    #muestra los campos del modelo
    list_display = ('pk','user','phone_number','website','picture')
    #enlaza los campos
    list_display_links =('pk','user')
    #Habilita los campos para que puedan editarse, sin entrar al perfil
    list_editable=('phone_number','website','picture')
    #Habilita los campos para que puedan ser buscado. se agrega automaticamente una barra de busqueda
    search_fields=('user__email','user__username','user__first_name','user__last_name','phone_number')
    #Filtro sobre los campos
    list_filter=('created','modified','user__is_active','user__is_staff')

    fieldsets = (
        ('Profile',{
            'fields':(('user','picture')),

        }),
        ('Extra Info',{
            'fields':(
                ('website' ,'phone_number'),
                ('biography')
        )
        }),
        ('Metadata',{
            'fields':(('created','modified'))
        })
       

    )
    readonly_fields=('created','modified')


class ProfileInLine(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural='Profiles'

class UserAdmin(BaseUserAdmin):
    inline = (ProfileInLine,)


admin.site.unregister(User)
admin.site.register(User,UserAdmin)