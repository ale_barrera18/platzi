
from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
#Se debe importar para poder utilizar, en la instacia de static/media
from django.conf import settings

from django.conf.urls import include,url
from django.conf.urls.static import static


urlpatterns = [
   url('admin/',admin.site.urls),
   url(r'ale/',include('ale.urls')),
   
    
] + static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
#permite guardar las imagenes en la carpeta media