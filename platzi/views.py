from django.http import HttpResponse

#Utilities
from datetime import datetime

def hello_world(request):

    now =datetime.now()
    return HttpResponse('hello world, Current server time is {now}'.format(now=str(now)))